#! /usr/bin/env python

"""An ordered list implementation.

09/2013
"""

__author__ = 'zhiqiu'


class OrderedListError(Exception):
    pass


class OrderedList(object):
    def __init__(self, capacity=-1, no_dup=True, dup_cmp=None, dup_key=None,
                 cmp_function=None, key_function=None, reverse=False,):
        """Creates an ordered list.

        The ordered list behaves like a priority queue but adds checking for
        duplicated elements. Its efficiency is not as good as a priority
        queue since it reorders the list upon insertion / deletion.

        The default ordering is that smaller elements are placed near head,
        unless reverse is set to True.

        Duplicated elements can be ignored by setting the no_dup argument.
        When enabled, dup_cmp and dup_key can be used to overwrite the
        behavior for checking duplicates. Only one of these two can be set.

        The default comparison behavior can be overwritten by either setting
        the cmp_function or the key_function parameters, but they cannot be
        set together.

        Args:
            capacity: The number of elements the queue can hold. The value -1
                means no limit.
            no_dup: Whether not to include duplicated elements.
            dup_cmp: A f(x, y) function that returns a boolean value
                indicating whether element x and y should be treated as
                equivalent when eliminating duplicates.
            dup_key: A f(x) function that returns a key; two elements with
                the same key are considered as equivalent when eliminating
                duplicates.
            reverse: When set to True, larger elements are placed near the
                head.
            cmp_function: When set to f(x, y) that returns int value,
                it used to compare x and y. The return value should be
                consistent with x - y when makes sense.
            key_function: When set to f(x) that returns int value, it will
                be used as key to evaluate an element.
        """
        if dup_cmp and dup_key:
            raise OrderedListError(
                'dup_cmp and dup_key cannot be set together!')

        if cmp_function and key_function:
            raise OrderedListError(
                'cmp_function and key_function cannot be set together!')

        self._capacity = capacity

        def is_dup(x, y):
            if dup_cmp:
                return dup_cmp(x, y)
            elif dup_key:
                return dup_key(x) - dup_key(y) == 0
            else:
                return x == y
        self._is_dup = is_dup if no_dup else lambda x, y: False

        flip = -1 if reverse else 1

        def local_cmp_function(x, y):
            if cmp_function:
                return flip * cmp_function(x, y)
            elif key_function:
                return flip * (key_function(x) - key_function(y))
            else:
                return flip * cmp(x, y)
        self._cmp = local_cmp_function

        self._list = []

    def heap_push(self, element, favor_newer=True):
        """Push and reorder.

        Args:
            element: The element to be pushed.
            favor_newer: If set to True, when two elements are considered to
                be duplicates and equivalent, the new one will be use.

        Returns:
            The element being evicted or None. The element being evicted
            could be the element at head or could be the duplicated element.
        """
        for i, elem in enumerate(self._list):
            if self._is_dup(element, elem):  # Duplicate found.
                cmp_result = self._cmp(element, elem)
                if cmp_result < 0 or (cmp_result == 0 and favor_newer):
                    # Delete the original.
                    self._list.pop(i)
                    break
                else:
                    # Reject element.
                    return element

        index = 0
        for i, elem in enumerate(self._list):  # No duplicates guaranteed.
            # If two elements have the same priorities, the new one is
            # placed behind the one already in the list.
            if self._cmp(element, elem) >= 0:
                index = i + 1
        self._list.insert(index, element)

        if self._capacity == -1 or len(self._list) <= self._capacity:
            return None
        else:
            return self._list.pop()

    def heap_pop(self):
        """Pop head."""
        return None if not self._list else self._list.pop(0)

    def heap_peek(self):
        """Peek head."""
        return None if not self._list else self._list[0]

    def size(self):
        return len(self._list)

    def is_empty(self):
        return len(self._list) == 0

    def iterate(self):
        """Iterating of the queue."""
        for elem in self._list:
            yield elem

    def extend(self, another_iterable, favor_newer=True):
        """Extends the current queue using elements from another iterable."""
        for elem in another_iterable:
            self.heap_push(elem, favor_newer=favor_newer)
