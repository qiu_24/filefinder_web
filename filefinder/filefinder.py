#! /usr/bin/env python

"""This searcher package searches the appearance of strings in files.

09/2013

TODO: Remove duplicates based on the processed content.

"""

__author__ = 'Zhi Qiu'

import collections
from itertools import product
import logging
from mimetypes import guess_type
import threading
from os import getcwd, listdir, path, walk
import re
from urllib2 import quote

from ordered_list import OrderedList

logging.basicConfig(level=logging.WARNING)

TIME_OUT = 6  # (sec), The following signal will be sent upon timeout.
STOP_SIGNAL = False  # Used in to stop the current search.

purple, green, blue, yellow, red, normal = (
    '\033[95m', '\033[92m', '\033[94m', '\033[93m', '\033[91m', '\033[0m')

# The compiled finders are used to file if a line of text contains a link.
link_matchers = [re.compile(r'.*(http://[^ \n]*)'),
                 re.compile(r'.*(https://[^ \n]*)')]

# SingleFinderResult stores a single searched result from regex within a string.
# Fields:
#   string: The actual matched string.
#   start: Start position of the match.
#   end: End position of the match.
SingleFinderResult = collections.namedtuple(
    'SingleFinderResult', ('string', 'start', 'end'))

# n: int, line number; str: str, line content
Line = collections.namedtuple('Line', ('line_number', 'text'))

# filename: Path and file of the file containing the stub.
# content, context_before, context_after: lists of Line objects.
TextStub = collections.namedtuple(
    'TextStub', ('filename', 'content', 'context_before', 'context_after'))

# A processed line of text.
# Fields:
#   text_match: The exact matched string (text span) in a line.
#   text_before, text_after: The string before and after the exact match in a
#       line. (Might be empty.)
#   n: Line number.
ProcessedLine = collections.namedtuple(
    'ProcessedContent',
    ('line_number', 'text_match', 'text_before', 'text_after'))


# SingleSearchResult stores a processed search result of a TextStub.
# Fields:
#   filename: Path and name of the file containing the stub.
#   exact_match_line_number: The line number of the first line contains exact
#       match text.
#   text_stub: The searched TextStub object.
#   processed_content: A list of ProcessedLine objects.
#   match_string: The whole matched string.
#   rank: The best rank of this search. Higher values means more suitable
#       results.
SingleProcessedTextStub = collections.namedtuple(
    'SingleProcessedTextStub',
    ('text_stub', 'processed_content', 'exact_match_line_number',
     'match_string', 'rank'))


def extract_text_stub(text_stream, filename='',
                      content_number_of_lines=3,
                      context_number_of_lines_above=2,
                      context_number_of_lines_below=2,
                      search_filename=True):
    """Iterates through a text stream and yield text stubs used for searching.

    A "content" is a list of strings to be searched, each corresponding to
    one line from text_stream. A "context" is a list of strings that contains
    the "content" and which provides the texts around it for better view.

    Args:
        text_stream: An iterable object that gives one line of text for each
            iteration.
        filename: Path and name of the file containing the text stub.
        content_number_of_lines: The number of lines a "content" includes up to.
        context_number_of_lines_above, context_number_of_lines_below: The
            additional number of lines a "context" will include besides
            "content".
        search_filename: Whether to search filename as well. Filename will be
            considered having line number 0.

    Yields:
        A TextStub object representing the text region that will be searched.
    """
    n, na, nb = (content_number_of_lines, context_number_of_lines_above,
                 context_number_of_lines_below)
    total = n + na + nb  # Total number of lines.
    text_buffer = []  # Will hold (line_number, line_content).
    for line_number, line_content in enumerate(text_stream, 1):  # From 1.
        # Update buffer.
        if len(text_buffer) < total:
            text_buffer.append(Line(line_number, line_content))
        else:
            text_buffer.append(Line(line_number, line_content))
            text_buffer.pop(0)

        # Extract content backwards using negative index convention.
        context_after = text_buffer[-nb:] if nb > 0 else []
        content = text_buffer[-nb-n:-nb] if nb > 0 else text_buffer[-n:]
        context_before = text_buffer[:-nb-n]
        if content:  # Non empty content rules the logic.
            yield TextStub(filename=filename, content=content,
                           context_before=context_before,
                           context_after=context_after)

    # Reaching end of file: continue taking contents by shrinking nb and n.
    while nb + n > 0:
        if nb > 0:
            nb -= 1
        else:
            n -= 1
        if len(text_buffer) > n+na+nb:
            text_buffer.pop(0)
        # Again uses negative index convention.
        context_after = text_buffer[-nb:] if nb > 0 else []
        # When n=0 this incorrectly gives whole line.
        content = text_buffer[-nb-n:-nb] if nb > 0 else text_buffer[-n:]
        context_before = text_buffer[:-nb-n]
        # Use n not content, for the text_buffer[-n:] issue mentioned above.
        if n:
            if content:
                yield TextStub(filename=filename, content=content,
                               context_before=context_before,
                               context_after=context_after)
        else:
            break

    # Finally, yield the filename.
    if search_filename:
        yield TextStub(filename=filename, content=[Line(0, filename)],
                       context_before=[], context_after=[])


def find_all_in_string(finder, string):
    """Find all matches in a string.

    Each match is represented in a SingleFinderResult object.

    Args:
        finder: A re.compile object.
        string: The string to be searched.

    Returns:
        A list of SingleFinderResult objects.
    """
    found_results = []
    find_from = 0
    while find_from < len(string):
        found_result = finder.search(string, find_from)
        if found_result:
            start, end = found_result.span()
            found_results.append(SingleFinderResult(string=found_result.group(),
                                                    start=start, end=end))
            find_from = end
        else:
            break
    return found_results


def keywords_to_finders(keywords, ignore_case=False, whole_word=False):
    """Converts keywords to regex finders.

    Args:
        keywords: A list of keyword strings. These keywords cannot contain
            regular expressions.
        ignore_case: Whether to ignore cases.
        word_only: Whether to only search for complete words. This feature is
            implemented by including the "\b" word boundary in the regular
            expression.

    Returns:
        A list of re.compile finders.
    """
    flags = 0
    if ignore_case:
        flags |= re.IGNORECASE

    if whole_word:
        word_only_transformer = lambda _: r'\b%s\b' % _
    else:
        word_only_transformer = lambda _: _

    return [re.compile(word_only_transformer(
        escape_special_character(x)), flags=flags) for x in keywords]


def rank_single_finder_result_list(single_finder_result_list, content):
    """Calculates a score for a list of SingleFinderResult objects.

    Args:
        single_finder_result: A list of SingleFinderResult objects; each entry
            coming from one finder.
        content: A TextStub.content for the text to be searched.

    Returns:
        A (int) rank for the search result. Higher rank corresponds to better
            search result.
    """
    weight = 100

    # First get the span of all the strings, which is the length of the left
    # most and right most word in the searched result.
    span_min = min(result.start for result in single_finder_result_list)
    span_max = max(result.end for result in single_finder_result_list)
    span = span_max - span_min

    weight -= span

    # Next consider the ordering of the keywords: the order they appear in
    # the keywords list indicate their desired appearance order in the
    # match_string, resulting in a higher rank if they are the same.
    locations = [(desired_index, result.start) for desired_index, result
                 in enumerate(single_finder_result_list)]

    # Sort it according to location, thus the index of the element in the new
    # list is the actual index of the words.
    locations.sort(key=lambda x: x[1])
    order_rank = sum(abs(desired_index - actual_index)
                     for actual_index, (desired_index, start)
                     in enumerate(locations))

    # Add it in.
    n = len(single_finder_result_list)
    relative_weight = 30.0/n/n

    weight -= relative_weight * order_rank

    return weight


def process_text_stub(text_stub, finders,
                      context_number_of_lines_above=None,
                      context_number_of_lines_below=None):
    """Calculates a best score for one searched TextStub result.

    Once the best matching is decided, the content will be squeezed to "just
    fit" those matching lines.

    Args:
        text_stub: A TextStub object that needs to be ranked.
        finders: A list of re.compile objects.
        context_number_of_lines_above: Number of lines for context above
            content. If not explicitly set, len(context_before) is used.
        context_number_of_lines_below: Number of lines for context below
            content. If not explicitly set, len(context_after) is used.

    Returns:
        A SingleProcessedTextStub object for the processed search result.
        Higher rank corresponds to smaller value. It returns None if the text
        stub is not a valid search result (its content does not contain all
        the keywords).
    """
    if context_number_of_lines_above is None:
        context_number_of_lines_above = len(text_stub.context_before)
    if context_number_of_lines_below is None:
        context_number_of_lines_below = len(text_stub.context_after)

    all_finder_results = []  # [[results from finder 1], ...]
    content = '\n'.join(one_line_content.text
                        for one_line_content in text_stub.content)
    for finder in finders:
        single_finder_result = find_all_in_string(finder, content)
        if single_finder_result:
            all_finder_results.append(single_finder_result)
        else:
            return None  # one of the keyword is not included in content

    best_rank = -1000000
    best_combination = None
    for finder_result_combination in product(*all_finder_results):
        if STOP_SIGNAL:  # Stops no matter what
            logging.info('Process text stub interrupted.')
            return None

        rank = rank_single_finder_result_list(finder_result_combination,
                                              text_stub.content)
        if rank > best_rank:
            best_combination = finder_result_combination
            best_rank = rank

    span_min = min(result.start for result in best_combination)
    span_max = max(result.end for result in best_combination)
    match_string = content[span_min:span_max]

    processed_content = []
    # The contexts before and after are adjusted because searched content may
    #  contains fewer lines than the passed-in content. The following two
    # lists holds context before and after the match in the original content.
    try_new_context_before, try_new_context_after = [], []
    new_content = []  # Adjusted(squeezed) new content.
    exact_match_line_number = None
    for one_line_content in text_stub.content:
        line_number = one_line_content.line_number
        content = one_line_content.text
        if span_min >= 0:
            text_before = content[:span_min]
            text_match = content[span_min:span_max]
            text_after = content[span_max:]
        elif span_max >= 0:
            text_before = ''
            text_match = content[:span_max]
            text_after = content[span_max:]
        else:
            text_before = ''
            text_match = ''
            text_after = content

        if not text_match:  # Not the matching line.
            if span_min >= 0:
                # Adds to new_context_before:
                try_new_context_before.append(Line(line_number, text_before))
            elif span_max <= 0:
                # Adds to new_context_after:
                try_new_context_after.append(Line(line_number, text_after))
        else:
            new_content.append(Line(line_number, content))
            processed_content.append(ProcessedLine(
                line_number=line_number, text_match=text_match,
                text_before=text_before, text_after=text_after))
            if not exact_match_line_number:
                exact_match_line_number = one_line_content.line_number

        span_min -= len(content) + 1  # +1 for '\n'
        span_max -= len(content) + 1

    # Re-adjust context.
    # before:
    try_new_context_before_size = len(try_new_context_before)
    if try_new_context_before_size >= context_number_of_lines_above:
        new_context_before = (
            try_new_context_before[-context_number_of_lines_above:])
    else:
        new_context_before = try_new_context_before
        diff = context_number_of_lines_above - try_new_context_before_size
        new_context_before[:0] = text_stub.context_before[-diff:]
    # after:
    try_new_context_after_size = len(try_new_context_after)
    if try_new_context_after_size >= context_number_of_lines_below:
        new_context_after = (
            try_new_context_after[:context_number_of_lines_below])
    else:
        new_context_after = try_new_context_after
        diff = context_number_of_lines_below - try_new_context_after_size
        new_context_after.extend(text_stub.context_after[:diff])

    new_text_stub = TextStub(
        filename=text_stub.filename, content=new_content,
        context_before=new_context_before, context_after=new_context_after)

    return SingleProcessedTextStub(
        text_stub=new_text_stub, processed_content=processed_content,
        exact_match_line_number=exact_match_line_number,
        match_string=match_string, rank=best_rank)


def initialize_search_result_list(capacity=-1):
    """Returns an empty search result ordered list.

    The list is ordered so that results with higher ranks are near the head.

    Args:
        capacity: Ordered list capacity.

    Returns:
        A OrderedList object that can store SingleProcessedTextStub objects.
    """
    def search_result_equivalent_cmp(r1, r2):
        return (r1.match_string == r2.match_string
                and r1.exact_match_line_number == r2.exact_match_line_number)

    return OrderedList(capacity=capacity, no_dup=True,
                       dup_cmp=search_result_equivalent_cmp, reverse=True,
                       key_function=lambda x: x.rank)


def search_text_stream(search_result_list, text_stream, finders,
                       filename='',
                       content_number_of_lines=1,
                       context_number_of_lines_above=1,
                       context_number_of_lines_below=1,
                       search_filename=True):
    """Searches text stream.

    Args:
        search_result_list: A OrderedList of SingleProcessedTextStub
            objects storing all searched results.
        text_stream: An iterable text object.
        finders: A list of re.compile finders.
        filename: Path and name of the file giving the text stream.
        content_number_of_lines: Number of lines for content.
        context_number_of_lines_above: Number of lines for context above
            content.
        context_number_of_lines_below: Number of lines for context below
            content.
        search_filename: Whether to search filename as well.

    Returns:
        The number of searched results.
    """
    for text_stub in extract_text_stub(
        text_stream, filename=filename,
        content_number_of_lines=content_number_of_lines,
        context_number_of_lines_above=context_number_of_lines_above,
        context_number_of_lines_below=context_number_of_lines_below,
        search_filename=search_filename):
        processed_text_stub = process_text_stub(
            text_stub, finders,
            context_number_of_lines_above=context_number_of_lines_above,
            context_number_of_lines_below=context_number_of_lines_below)
        if processed_text_stub:
            search_result_list.heap_push(processed_text_stub)
    return search_result_list.size()


def search_text_stream_new_result_list(
        max_number_of_results, text_stream, finders, filename='',
        content_number_of_lines=1, context_number_of_lines_above=1,
        context_number_of_lines_below=1, search_filename=True):
    """This version of search_text_stream returns a new search result list."""
    search_result_list = initialize_search_result_list(
        capacity=max_number_of_results)
    search_text_stream(
        search_result_list, text_stream, finders, filename=filename,
        content_number_of_lines=content_number_of_lines,
        context_number_of_lines_above=context_number_of_lines_above,
        context_number_of_lines_below=context_number_of_lines_below,
        search_filename=search_filename)
    return search_result_list


def search_text_file(search_result_list, filename, finders,
                     content_number_of_lines=5,
                     context_number_of_lines_above=2,
                     context_number_of_lines_below=2,
                     search_filename=True,
                     treat_unknown_as_text=True):
    """Search text.

    Args:
        search_result_list: A OrderedList of SingleProcessedTextStub
            objects storing all searched results.
        filename: Path and name to the file that will be searched.
        finders: A list of re.compile finders.
        content_number_of_lines: Max number of lines for content. Each search
            result satisfies that all keywords are within this number of lines
            of text.
        context_number_of_lines_above: Number of lines for context above
            content.
        context_number_of_lines_below: Number of lines for context below
            content.
        search_filename: Whether to include filename in the search.
        treat_unknown_as_text: Whether to treat unknown type of files as text
            files.

    Returns:
        The total number of searched results. Note that this is a "raw count"
        as there might be duplicated results.
        Returns negative numbers if error happens:
            -1: If the file is not a text file.
    """
    if not is_text_file(filename, treat_unknown_as_text=treat_unknown_as_text):
        return -1
    text = [line.strip() for line in file(filename)]

    return search_text_stream(
        search_result_list, text, finders, filename=filename,
        content_number_of_lines=content_number_of_lines,
        context_number_of_lines_above=context_number_of_lines_above,
        context_number_of_lines_below=context_number_of_lines_below,
        search_filename=search_filename)


def search_text_file_new_result_list(
    max_number_of_results, filename, finders,
    content_number_of_lines=5, context_number_of_lines_above=2,
    context_number_of_lines_below=2, search_filename=True,
    treat_unknown_as_text=True):
    """This version calls search_text_stream_new_result_list."""
    if not is_text_file(filename, treat_unknown_as_text=treat_unknown_as_text):
        return -1
    text = [line.strip() for line in file(filename)]

    return search_text_stream_new_result_list(
        max_number_of_results, text, finders, filename=filename,
        content_number_of_lines=content_number_of_lines,
        context_number_of_lines_above=context_number_of_lines_above,
        context_number_of_lines_below=context_number_of_lines_below,
        search_filename=search_filename)


def search_text_files(search_result_list, filenames, finders,
                      content_number_of_lines=5,
                      context_number_of_lines_above=2,
                      context_number_of_lines_below=2,
                      search_filename=True,
                      treat_unknown_as_text=True):
    for filename in filenames:
        if STOP_SIGNAL:
            # Stops no matter what.
            logging.info('Search text files interrupted.')
            return

        search_text_file(
            search_result_list, filename, finders,
            content_number_of_lines=content_number_of_lines,
            context_number_of_lines_above=context_number_of_lines_above,
            context_number_of_lines_below=context_number_of_lines_below,
            search_filename=search_filename,
            treat_unknown_as_text=treat_unknown_as_text)


def is_text_file(filename, treat_unknown_as_text=True):
    """Checks if a file is a text file.

    Args:
        filename: Path and name of the file to be checked.

    Returns:
        A boolean value for whether the file is a text file.
    """
    try_guess_type = guess_type(filename)[0]
    if try_guess_type is not None:
        return try_guess_type.split('/')[0] == 'text'
    else:
        return treat_unknown_as_text


def walk_directory(root_directory=getcwd(), root_only=False):
    """Yields files under the directory.

    By default the walk includes subdirectories, unless the root_only
    parameter is set to True.

    Args:
        directory: The directory to list.
        root_only: When set to True, no sub-directories are walked.

    Yields:
        The full path for each of the files.
    """
    if root_only:
        for relative_path in listdir(root_directory):
            if relative_path.startswith('.'):
                continue
            full_path = path.join(root_directory, relative_path)
            if path.isfile(full_path):
                yield full_path
    else:
        for base_directory, dir_names, file_names in walk(root_directory):
            for filename in file_names:
                if filename.startswith('.'):
                    continue
                full_path = path.join(base_directory, filename)
                yield full_path


def search_directory_DEPRECATED(
    keywords, ignore_case=False, whole_word_only=True,
    root_directory=getcwd(), root_only=False,
    content_number_of_lines=5,
    context_number_of_lines_above=2,
    context_number_of_lines_below=2,
    max_number_of_results=6,
    search_filename=True,
    treat_unknown_as_text=True):
    """Searches files under root_directory.

    Search files under root_directory for all keywords. All found results are
    ranked and only the top max_number_of_results are returned. All contents
    are within content_number_of_lines number of lines.

    Args:
        keywords: A list of strings to be searched.
        ignore_case: Whether to ignore cases.
        whole_word_only: Whether to search only for complete words.
        root_directory: Files under this directory will be searched.
        root_only: Whether to ignore subdirectories.
        content_number_of_lines: Max number of lines for content. Each search
            result satisfies that all keywords are within this number of lines
            of text.
        context_number_of_lines_above: Number of lines for context above
            content.
        context_number_of_lines_below: Number of lines for context below
            content.
        max_number_of_results: Number of results to return, see above.
        search_filename: Whether to search filename as well.
        treat_unknown_as_text: Whether to treat unknown type files as texts.

    Returns:
        A OrderedList of SingleProcessedTextStub objects containing all
        top-ranked results.
    """
    finders = keywords_to_finders(keywords, ignore_case=ignore_case,
                                  whole_word=whole_word_only)
    search_results = initialize_search_result_list(
        capacity=max_number_of_results)

    for filename in walk_directory(root_directory=root_directory,
                                   root_only=root_only):
        search_text_file(
            search_results, filename, finders,
            content_number_of_lines=content_number_of_lines,
            context_number_of_lines_above=context_number_of_lines_above,
            context_number_of_lines_below=context_number_of_lines_below,
            search_filename=search_filename,
            treat_unknown_as_text=treat_unknown_as_text)

    return search_results


def search_directory_parallel(
    keywords, pool_size=10, ignore_case=False, whole_word_only=True,
    root_directory=getcwd(), root_only=False, content_number_of_lines=5,
    context_number_of_lines_above=2, context_number_of_lines_below=2,
    max_number_of_results=6, search_filename=True, treat_unknown_as_text=True):
    """Searches files under root_directory in parallel.

    Search files under root_directory for all keywords. All found results are
    ranked and only the top max_number_of_results are returned. All contents
    are within content_number_of_lines number of lines.

    Args:
        keywords: A list of strings to be searched.
        pool_size: Size of worker pool.
        ignore_case: Whether to ignore cases.
        whole_word_only: Whether to search only for complete words.
        root_directory: Files under this directory will be searched.
        root_only: Whether to ignore subdirectories.
        content_number_of_lines: Max number of lines for content. Each search
            result satisfies that all keywords are within this number of lines
            of text.
        context_number_of_lines_above: Number of lines for context above
            content.
        context_number_of_lines_below: Number of lines for context below
            content.
        max_number_of_results: Number of results to return, see above.
        search_filename: Whether to search filename as well.
        treat_unknown_as_text: Whether to treat unknown type files as texts.

    Returns:
        A OrderedList of SingleProcessedTextStub objects containing all
        top-ranked results.
    """
    global STOP_SIGNAL
    STOP_SIGNAL = False

    finders = keywords_to_finders(keywords, ignore_case=ignore_case,
                                  whole_word=whole_word_only)
    search_results = initialize_search_result_list(
        capacity=max_number_of_results)

    parallel_search_results = [initialize_search_result_list(
        capacity=max_number_of_results) for _ in range(pool_size)]

    filename_groups = [[] for _ in range(pool_size)]
    for i, filename in enumerate(walk_directory(
        root_directory=root_directory, root_only=root_only)):
        filename_groups[i % pool_size].append(filename)

    def search_fn(filenames, pid):
        logging.info('Pid %s started.', pid)
        search_text_files(
            parallel_search_results[pid], filenames, finders,
            content_number_of_lines=content_number_of_lines,
            context_number_of_lines_above=context_number_of_lines_above,
            context_number_of_lines_below=context_number_of_lines_below,
            search_filename=search_filename,
            treat_unknown_as_text=treat_unknown_as_text)
        logging.info('Pid %s ended.', pid)

    threads = [threading.Thread(target=search_fn, args=[filenames, i]) for
        i, filenames in enumerate(filename_groups)]

    # Starts a timer before search starts.
    def time_up():
        global STOP_SIGNAL
        STOP_SIGNAL = True
    timer = threading.Timer(TIME_OUT, time_up)
    timer.start()

    # Starts search now.
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()

    # No need to keep the timer.
    timer.cancel()

    # Combine search results.
    for one_search_result_list in parallel_search_results:
        search_results.extend(one_search_result_list.iterate())

    return search_results


def processed_text_stub_to_pretty_text(processed_text_stub,
                                       check_link=True):
    """Returns a pretty string for the given process_text_stub.

    Args:
        processed_text_stub: A SingleProcessedTextStub object.
        check_link: If set to True, check if a file has a master link and
            print out the link.

    Returns:
        A string containing color for pretty printing in terminal. The
        returned string contains all information and is ready to be printed.
    """
    def format_line_number(line_number):
        return ' %s%5d%s' % (blue, line_number, normal)

    text = list()

    # Header:
    text.append(
        '%s %s' %
        (yellow + 'file://' + quote(processed_text_stub.text_stub.filename),
        green + 'line number: ' +
        str(processed_text_stub.exact_match_line_number)))

    # Check master link:
    if check_link:
        master_link = get_master_link(processed_text_stub.text_stub.filename)
        if master_link:
            text.append('(See %s)' % (green + master_link))

    # Pre-context:
    for line in processed_text_stub.text_stub.context_before:
        text.append('%s: %s' %
                    (format_line_number(line.line_number), line.text))
    # Content:
    for processed_line in processed_text_stub.processed_content:
        text.append('%s: %s%s%s' %
                    (format_line_number(processed_line.line_number),
                    normal + processed_line.text_before + normal,
                    red + processed_line.text_match + normal,
                    normal + processed_line.text_after + normal))
    # Post-context:
    for line in processed_text_stub.text_stub.context_after:
        text.append('%s: %s' %
                    (format_line_number(line.line_number), line.text))

    return '\n'.join(text)


def get_master_link(filename):
    """Returns the master link of a file.

    A master link is the link associated to a file; currently it is defined
    to be the link existing in the first line of the file if any.

    Args:
        filename: Path and name of a file.

    Returns:
        The master link or None.

    """
    with file(filename) as fp:
        line_one = fp.readline()
        for matcher in link_matchers:
            result = matcher.match(line_one)
            if result:
                return result.group(1)
        else:
            return None


def escape_special_character(string):
    """Escapes special characters in string.

    Args:
        string: A string which may contain special characters.

    Returns:
        A string with special characters escaped.
    """
    new_string_list = []
    for char in string:
        if char in ('.', '(', ')', '[', ']', '{', '}',
                    '^', '$', '*', '?', '+'):
            new_string_list.append('\\' + char)
        else:
            new_string_list.append(char)
    return ''.join(new_string_list)


def simple_interface():
    """Simple search interface.

    This interface simply takes positional args as a list of keywords for a
    search.

    """
    import argparse
    parser = argparse.ArgumentParser(
        description='Search for files containing the specified strings.')
    parser.add_argument('keywords', nargs='+')
    parser.add_argument('-cs', '--content_size', type=int,
                        help='content may contain up to this number of lines.')
    parser.add_argument('-c', '--context', type=int,
                        help='number of lines around the match')
    parser.add_argument('-l', '--limit', type=int,
                        help='limit the number of results to')
    parser.add_argument('-w', '--word', action='store_true',
                        help='whether to search only for complete words')
    parser.add_argument('-nw', '--no-word', dest='word', action='store_false',
                        help='whether to search only for complete words')
    parser.add_argument('-r', '--recursive', action='store_true',
                        help='whether to search all files in all '
                             'sub-directories')
    parser.add_argument('-nr', '--no-recursive', dest='recursive',
                        action='store_false',
                        help='whether to search all files in all '
                             'sub-directories')
    parser.add_argument('-ic', '--ignore_case', action='store_true',
                        help='whether to ignore cases')
    parser.add_argument('-nic', '--no-ignore_case', dest='ignore_case',
                        action='store_false',
                        help='whether to ignore cases')
    parser.add_argument('-sf', '--search_filename', action='store_true',
                        help='whether to include filename in the search')
    parser.add_argument('-nsf', '--no-search_filename', dest='ignore_case',
                        action='store_false',
                        help='whether to include filename in the search')
    parser.add_argument('-tuat', '--treat_unknown_as_text',
                        action='store_true',
                        help='whether to treat unknown type files as texts '
                             '(warning: some binary file may appear to be of '
                             'unknown type)')
    parser.add_argument('-ntuat', '--no-treat_unknown_as_text',
                        dest='treat_unknown_as_text', action='store_false',
                        help='whether to treat unknown type files as texts  '
                             '(use this if you may have binary files inside '
                             'the search directory and searching is taking '
                             'unreasonably long)')
    parser.add_argument('-ps', '--pool_size', type=int,
                        help='size of pool for parallel searching')
    parser.add_argument('-log', '--logtostdout', action='store_true',
                        help='whether to log to stdout')
    parser.add_argument('-nolog', '--no-logtostdout', dest='log',
                        action='store_false', help='whether to log to stdout')

    parser.set_defaults(content_size=5, context=2, pool_size=1,
                        limit=5, word=False, recursive=True, ignore_case=True,
                        search_filename=True, treat_unknown_as_text=True,
                        logtostdout=False)
    args = parser.parse_args()

    if args.logtostdout:
        logging.basicConfig(level=logging.INFO)

    search_results = search_directory_parallel(
        args.keywords, pool_size=args.pool_size,
        ignore_case=args.ignore_case, whole_word_only=args.word,
        root_directory=getcwd(), root_only=(not args.recursive),
        content_number_of_lines=args.content_size,
        context_number_of_lines_above=args.context,
        context_number_of_lines_below=args.context,
        max_number_of_results=args.limit,
        search_filename=args.search_filename,
        treat_unknown_as_text=args.treat_unknown_as_text)

    if not search_results.is_empty():
        print(green + r"""
              ____ __ __     ____  ____ __ __  __ ____    ____ ____
             ||    || ||    ||    ||    || ||\ || || \\  ||    || \\
             ||==  || ||    ||==  ||==  || ||\\|| ||  )) ||==  ||_//
             ||    || ||__| ||___ ||    || || \|| ||_//  ||___ || \\
            """)
        for processed_text_stub in search_results.iterate():
            print(processed_text_stub_to_pretty_text(processed_text_stub))
    else:
        print('filefinder: Nothing found.')


interface = simple_interface

if __name__ == '__main__':
    interface()
