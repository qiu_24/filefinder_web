import logging
import os
import sys


# Append filefinder module path to PYTHONPATH.
lib_path = os.path.abspath("../filefinder/")
if lib_path not in sys.path:
    sys.path.append(lib_path)

# Log to stdout.
logging.basicConfig(file=sys.stdout, level=logging.DEBUG)
