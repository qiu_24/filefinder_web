from django.conf.urls import patterns, url
import urllib2

import views

urlpatterns = patterns(
    '',
    url(r'^$', views.home, name='homepage'),
    url(r'^homepage', views.home, name='homepage'),
    url(r'^search', views.search, name='search_page'),
)
