"""This module bridges filefinder and the searcher app."""

import filefinder
import re
import settings

NOTES_DIR = settings.NOTES_DIR

# Pre-compiled and used in decorate_link.
LINK_MATCHER = re.compile(r'(.*)((?:http|https)://[^ "\']*[^ "\'.(){}\[\]]+)(.*)')


def search_directory_web_interface(request):
    """Web search interface.

    Args:
        request: The request sent to functions in the views module.

    Returns:
        A list of html text paragraphs, each for one search result.
    """
    # TODO: Wrap into try and add error / help page.
    keywords = get_param(request, 'keywords', '', str)
    keywords = keywords.split(',')
    content_size = get_param(request, 'content_size', 5, int)
    context = get_param(request, 'context', 2, int)
    limit = get_param(request, 'limit', 5, int)
    pool_size = get_param(request, 'pool_size', 1, int)
    whole_word_only = get_param(request, 'whole_word_only', 0, bool)
    recursive = get_param(request, 'recursive', 1, bool)
    ignore_case = get_param(request, 'ignore_case', 1, bool)
    search_filename = get_param(request, 'search_filename', 1, bool)
    treat_unknown_as_text = get_param(
        request, 'treat_unknown_as_text', 1, bool)

    search_results = filefinder.search_directory_parallel(
        keywords, pool_size=pool_size,
        ignore_case=ignore_case, whole_word_only=whole_word_only,
        root_directory=NOTES_DIR, root_only=(not recursive),
        content_number_of_lines=content_size,
        context_number_of_lines_above=context,
        context_number_of_lines_below=context,
        max_number_of_results=limit,
        search_filename=search_filename,
        treat_unknown_as_text=treat_unknown_as_text)

    return [processed_text_stub_to_marked_text(processed_text_stub) for
            processed_text_stub in search_results.iterate()]


def get_param(request, param, default, type):
    """Parse a url parameter.

    Args:
        request: WSGIRequest request object.
        param: (string) Url param.
        default: Default value of the param before the conversion.
        type: Use to convert the url string value.

    Returns:
        Value for the param.
    """
    if type == bool:
      return type(int(request.GET.get(param, default)))
    else:
      return type(request.GET.get(param, default))


def processed_text_stub_to_marked_text(processed_text_stub,
                                       check_link=True):
    """Returns a marked html for the given process_text_stub.

    Modified from filefinder.processed_text_stub_to_pretty_text. It is
    assumed that each line contains at most one link.

    Args:
        processed_text_stub: A SingleProcessedTextStub object.
        check_link: If set to True, check if a file has a master link and
            print out the link.

    Returns:
        Html with different components of the text set to different classes.
    """
    def format_line_number(line_number):
        return '<span class="line_number_general">%s:</span>' % line_number

    # Header:
    filename_block = """
<span class="single_line">
    <span class="filename">file: %s</span>
    line number: <span class="line_number_hit">%d</span>
</span>""" % (processed_text_stub.text_stub.filename,
              processed_text_stub.exact_match_line_number)

    # Check master link:
    master_link_block = ''
    if check_link:
        master_link = filefinder.get_master_link(
            processed_text_stub.text_stub.filename)
        if master_link:
            master_link_block = """
<span class="master_link">
    <a href="%s">(web source)</a>
</span>""" % master_link

    fileheader_block = """
<div class="file_header">
    %s
    %s
</div>""" % (filename_block, master_link_block)

    # File content:
    # Pre-context:
    precontext_block = ''
    for line in processed_text_stub.text_stub.context_before:
        precontext_block += """
<div class="single_line">
    %s
    <span class="line_text">%s</span>
</div>""" % (format_line_number(line.line_number), decorate_link(line.text))
    # Content:
    content_block = ''
    for processed_line in processed_text_stub.processed_content:
        content_block += """
<div class="single_line">
    %s
    <span class="line_text">%s</span>
    <span class="line_text_hit">%s</span>
    <span class="line_text">%s</span>
</div>""" % (
            format_line_number(processed_line.line_number),
            decorate_link(processed_line.text_before),
            decorate_link(processed_line.text_match),
            decorate_link(processed_line.text_after))
    # Post-context:
    postcontext_block = ''
    for line in processed_text_stub.text_stub.context_after:
        postcontext_block += """
<div class="single_line">
    %s
    <span class="line_text">%s</span>
</div>""" % (format_line_number(line.line_number), decorate_link(line.text))

    filecontent_block = """
<div class="file_content">
    %s
    %s
    %s
</div>""" % (precontext_block, content_block, postcontext_block)

    return fileheader_block + filecontent_block


def decorate_link(text):
    """Decorate the last links in text by <a>."""
    found_link = LINK_MATCHER.search(text)
    if found_link and found_link.groups():
        return '%s<a href=%s>%s</a>%s' % (
            found_link.group(1), found_link.group(2), found_link.group(2),
            found_link.group(3))
    else:
        return text

