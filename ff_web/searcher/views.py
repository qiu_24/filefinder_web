from django.http import HttpResponse
from django.template import RequestContext, loader

import bridger


def home(request):
    template = loader.get_template('homepage.tmpl')
    context = RequestContext(request, {
        'use_page_header': True,
        'content_choices': range(21),
        'content_default': 12,
        'pool_size_choices': range(1, 33),
        'pool_size_default': 1,
        'context_choices': range(31),
        'context_default': 9,
        'limit_choices': range(1, 31),
        'limit_default': 10,
    })

    return HttpResponse(template.render(context))


def search(request):
    search_results = bridger.search_directory_web_interface(request)

    template = loader.get_template('paragraph_list.tmpl')
    context = RequestContext(request, {
        'paragraphs': search_results,
    })

    return HttpResponse(template.render(context))
